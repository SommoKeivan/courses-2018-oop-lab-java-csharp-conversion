package it.unibo.oop.util;

import java.util.List;
import java.util.stream.Stream;

interface BaseMatrix<E> extends Iterable<E> {
    int getColumnsSize();
    int getRowsSize();
    int size();

    int coordDiagonal(int i, int j);
    int coordAntidiagonal(int i, int j);

    int coordRow(int d, int a);
    int coordColumn(int d, int a);

    E get(int i, int j);
    E getDiagonals(int d, int a);

    boolean contains(E element);

    Stream<E> getRow(int i);
    Stream<E> getColumn(int i);
    Stream<E> getDiagonal(int d);
    Stream<E> getAntidiagonal(int a);

    void forEachIndexed(BiIntObjConsumer<E> consumer);

    Object[] toArray();
    E[] toArray(E[] ts);
    List<E> toList();
}
