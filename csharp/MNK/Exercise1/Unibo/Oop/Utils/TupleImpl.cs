﻿using System;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        public readonly object[] items;

        public TupleImpl(params object[] args)  
        {
            this.items = args;
        }

        public object this[int i] => this.items[i];

        public int Length => items.Length;

        public object[] ToArray()
        {
            return (object[])this.items.Clone();
        }
        
        public override string ToString()
        {
            string finalString = "";
            if(items.Length > 0)
            {
                finalString += "(";
                for(int j = 0; j < items.Length; j++)
                {
                    finalString += this[j].ToString();
                    if (j != (items.Length - 1))
                    {
                        finalString += ", ";
                    }
                    else
                        finalString += ")";
                }
            }
            return finalString;
        }

        public override int GetHashCode()
        {
            const int prime = 31;
            int result = 1;
            foreach(object obj in items)
            {
                result = prime * result + obj.GetHashCode();
            }
            return result;
        }

        public override bool Equals(object obj)
        {
            return obj != null
                   && obj is TupleImpl
                   && this.items.SequenceEqual(((TupleImpl)obj).items);
        }
    }
}
