using System;

namespace Unibo.Oop.Utils
{
    public interface IMatrix<TElem> : IBaseMatrix<TElem>
    {
        new TElem this[int i, int j] { get; set; }

        void SetDiagonals(int d, int a, TElem value);

        void SetAll(TElem element);

        void SetAll(Func<int, int, TElem, TElem> setter);
    }
}